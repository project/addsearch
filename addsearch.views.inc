<?php

/**
 * @file
 * Contains viwws settings.
 */

/**
 * Implements hook_views_data().
 */
function addsearch_views_data() {

  $basefields = [
    'id',
    'url',
    'title',
    'meta_description',
    'meta_categories',
    'highlight',
    'ts',
    'categories',
    'document_type',
    'custom_fields',
    'score',
  ];

  // Base data.
  $data['addsearch']['table']['group'] = t('addsearch');
  $data['addsearch']['table']['base'] = [
    'title' => t('Addsearch'),
    'help' => t('Addsearch'),
    'query_id' => 'addsearch_query',
  ];

  foreach ($basefields as $value) {
    $data['addsearch'][$value] = [
      'title' => $value,
      'help' => $value,
      'field' => [
        'id' => 'addsearch_title',
      ],
    ];
  }

  $data['addsearch']['images'] = [
    'title' => t('Image'),
    'help' => t('Images that are indexed to addsearch.'),
    'field' => [
      'id' => 'addsearch_image',
    ],
  ];

  $data['addsearch']['term'] = [
    'title' => t('Search keyword'),
    'help' => t('This plugin is mostly needed one.'),
    'filter' => [
      'id' => 'addsearch_term',
    ],
  ];

  $data['addsearch']['sort'] = [
    'title' => t('SortOrder filter'),
    'help' => t('This plugin is mostly needed one.'),
    'filter' => [
      'id' => 'addsearch_sort_type',
    ],
  ];

  $data['addsearch']['lang'] = [
    'title' => t('Language filter'),
    'help' => t('Get only specific language items.'),
    'filter' => [
      'id' => 'addsearch_lang',
    ],
  ];

  $data['addsearch']['entity'] = [
    'title' => 'entity',
    'help' => 'help',
    'field' => [
      'id' => 'addsearch_url_to_entity',
    ],
  ];

  $addSearchService = \Drupal::service('addsearch.restapiservice');
  $enabledEntities = $addSearchService->getEnabledBundles();

  foreach ($enabledEntities as $entity => $settings) {
    $pointer = 'customfield_' . mb_strtolower($settings['name']);
    $data['addsearch'][$pointer] = [
      'title' => t('Filter by @type type', ['@type' => $entity]),
      'help' => t('This add custom filter to @type type.', ['@type' => $entity]),
      'filter' => [
        'id' => 'addsearch_sort_by_bundle',
      ],
    ];
  }

  $data['addsearch']['term']['argument'] = [
    'title' => t('Argument Term'),
    'help' => t('Provide term from contextual.'),
    'id' => 'term_arg_addsearch',
    'name field' => 'title',
    'numeric' => false,
    'validate type' => 'nid',
  ];


  return $data;
}
