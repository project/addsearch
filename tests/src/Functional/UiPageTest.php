<?php

namespace Drupal\Tests\addsearch\Functional;

/**
 * Tests that the Addsearch Admin pages are reachable.
 *
 * @group addsearch
 */
class UiPageTest extends AddsearchTestBase {

  /**
   * Tests basics of the visible data on this module..
   */
  public function testDataAppearing() {

    $this->drupalLogin($this->user);

    // We got the permissions.
    $this->drupalGet('/admin/config/search/addsearch');
    $this->assertSession()->statusCodeEquals(200);

    // Create addsearch settings.
    $edit = [];
    $edit['publish_time'] = TRUE;
    $edit['bundles[node][enabled]'] = TRUE;
    $edit['bundles[node][name]'] = 'type';
    $edit['machine_bundle_names'] = FALSE;

    // Form saving.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    // Get the node and its time.
    $node = $this->createMyNode();
    $creationTime = \Drupal::service('date.formatter')->format($node->getCreatedTime(), 'custom', 'Y-m-d H:i');

    $this->drupalGet('/node/' . $node->id());

    // Check that type is coming nicely.
    $addsearchCustomField = $this->xpath('//meta[@name="addsearch-custom-field"]');
    $this->assertEquals($addsearchCustomField[0]->getAttribute('content'), 'type=Article', 'Node got addsearch custom fields.', 'Addsearch');

    // Check that we got creation time.
    $createdTime = $this->xpath('//meta[@property="article:published_time"]');
    $this->assertEquals($createdTime[0]->getAttribute('content'), $creationTime, 'Creation time check', 'Addsearch');

  }

  /**
   * Create Expense node.
   */
  public function createMyNode() {
    // Populate defaults array.
    $settings = [
      'title' => $this->randomMachineName(8),
      'type' => 'article',
      'uid' => \Drupal::currentUser()->id(),
    ];

    return $this->drupalCreateNode($settings);
  }

}
