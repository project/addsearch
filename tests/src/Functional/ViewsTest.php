<?php

namespace Drupal\Tests\addsearch\Functional;

use Drupal\user\Entity\User;


/**
 * Tests that the Addsearch Views acts like intendent.
 *
 * @group addsearch
 */
class ViewsTest extends AddsearchTestBase {

/**
   * {@inheritdoc}
   */
  public static $modules = [
    // Modules for core functionality.
    'block',
    'field',
    'field_ui',
    'help',
    'node',
    'user',

    // Views. Duh. Enable the Views UI so it can be fully tested.
    'views',
    'views_ui',

      // This module.
    'addsearch'
  ];

  /**
   * Log in as user 1.
   */
  protected function loginUser1() {
    // Load user 1.
    /* @var \Drupal\user\Entity\User $account */
    $account = User::load(1);

    // Reset the password.
    $password = 'foo';
    $account->setPassword($password)->save();

    // Support old and new tests.
    $account->passRaw = $password;
    $account->pass_raw = $password;

    // Login.
    $this->drupalLogin($account);
  }

  /**
   * Tests basics of the visible data on this module..
   */
  public function testDataAppearing() {

    // Log in as user 1.
    $this->loginUser1();

    // We got the permissions.
    $this->drupalGet('/admin/config/search/addsearch');
    $this->assertSession()->statusCodeEquals(200);

    // Create addsearch settings.
    $edit = [];
    $edit['publish_time'] = TRUE;
    $edit['bundles[node][enabled]'] = TRUE;
    $edit['bundles[node][name]'] = 'type';
    $edit['machine_bundle_names'] = FALSE;

    // Form saving.
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet('/admin/structure/views/add');
    $this->assertSession()->statusCodeEquals(200);

    $edit = [];
    $edit['label'] = $edit['id'] = 'addsearch';
    $edit['show[wizard_key]'] = 'standard:addsearch';

    // Form saving.
    $this->submitForm($edit, 'Save and edit');
    $this->assertSession()->statusCodeEquals(200);

  }

}
