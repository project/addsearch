<?php

namespace Drupal\Tests\addsearch\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Sets up page and article content types.
 */
abstract class AddsearchTestBase extends BrowserTestBase {


  /**
   * User with permission..
   *
   * @var object
   */
  protected $user;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'addsearch', 'addsearch_purge'];

  /**
   * The node access control handler.
   *
   * @var \Drupal\Core\Entity\EntityAccessControlHandlerInterface
   */
  protected $accessHandler;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'bypass node access',
      'administer content types',
      'administer addsearch settings',
    ]);

    $this->drupalCreateContentType(['name' => 'page', 'type' => 'page']);
    $this->drupalCreateContentType(['name' => 'Article', 'type' => 'article']);
  }


}
