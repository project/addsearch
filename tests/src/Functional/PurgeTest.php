<?php

namespace Drupal\Tests\addsearch\Functional;

/**
 * Tests that the Rules UI pages are reachable.
 *
 * @group addsearch
 */
class PurgeTest extends AddsearchTestBase {

  /**
   * Tests basics of the visible data on this module..
   */
  public function testPurgeQueue() {

    $this->drupalLogin($this->user);

    // We got the permissions.
    $this->drupalGet('/admin/config/search/addsearch');

    // Create addsearch settings and groups.
    $edit = [];
    $edit['publish_time'] = TRUE;
    $edit['bundles[node][enabled]'] = TRUE;
    $edit['bundles[node][name]'] = 'type';
    $edit['machine_bundle_names'] = FALSE;

    //var_dump(print_r($edit, TRUE));
    $this->submitForm($edit, 'Save configuration');

    $nodes = [];
    $queueSize = mt_rand(5,15);

    // Lets create nodes so that we got queue
    for($i=0; $i < $queueSize; $i++) {
      $nodes[] = $this->createMyNode();
    }

    $queue_factory = \Drupal::service('queue');
    $queue = $queue_factory->get('addsearch_queueworker');


    $connection = \Drupal::database();
    $item = $connection->query('SELECT * FROM {' . $queue::TABLE_NAME . '} q WHERE name = :name ORDER BY expire ASC', [
      ':name' => 'addsearch_queueworker',
    ])->fetchAll();

    // Check there is all in queue.
    $this->assertEquals(count($item), $queueSize, 'Everything is purged', 'Addsearch');

    $lastItem = end($item)->expire;
    $firstItem = reset($item)->expire;

    $HowManyQueries = ($lastItem - $firstItem) / count($item);

    $this->assertTrue($HowManyQueries > 9, "Atleast 9 second difference.");

  }

  /**
   * Create Expense node.
   */
  public function createMyNode() {
    // Populate defaults array.
    $settings = [
      'title' => $this->randomMachineName(8),
      'type' => 'article',
      'uid' => \Drupal::currentUser()->id(),
    ];

    return $this->drupalCreateNode($settings);
  }


}
