(function ($, Drupal, drupalSettings) {

  var sendStats = function(pubkey, data) {
    if (typeof window !== 'undefined' && window.navigator && window.navigator.sendBeacon) {
      navigator.sendBeacon('https://api.addsearch.com/v1/stats/' + pubkey + '/', JSON.stringify(data));
    }
    else {
      fetch('https://api.addsearch.com/v1/stats/' + pubkey + '/', {
          method: 'POST',
          headers: {
            'Content-Type': 'text/plain',
          },
          body: JSON.stringify(data)
      });
    }
  }

  Drupal.behaviors.autocomplete_clickevents = {
    attach: function (context) {
      const elements = context.querySelectorAll('[data-addsearchrowtracking]');
      if (elements) {
        elements.forEach( el => {
          try {
            if (el.dataset && el.dataset.addsearchrowtracking) {
              const parsed = JSON.parse(el.dataset.addsearchrowtracking);
              el.removeAttribute('data-addsearchrowtracking');
              const links = el.querySelectorAll('a');
              if(!links) {
                return;
              }

              links.forEach( link => {
                 link.addEventListener('click', (e) => {
                  sendStats(drupalSettings.addsearch.pubkey, {
                     "action": "click",
                     "keyword": parsed.keyword,
                     "docid": parsed.id,
                     "position": parsed.position
                  });
                })
              })

            }
          }
          catch(e){
            console.log('error',e)
          }
        });
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
