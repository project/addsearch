(function ($, drupalSettings) {

  // Credits: https://stackoverflow.com/a/5717133
  function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
  }

  var func = Drupal.behaviors.autocomplete.attach;

  var sendStats = function(pubkey, data) {
    if (typeof window !== 'undefined' && window.navigator && window.navigator.sendBeacon) {
      navigator.sendBeacon('https://api.addsearch.com/v1/stats/' + pubkey + '/', JSON.stringify(data));
    }
    else {
      fetch('https://api.addsearch.com/v1/stats/' + pubkey + '/', {
          method: 'POST',
          headers: {
            'Content-Type': 'text/plain',
          },
          body: JSON.stringify(data)
      });
    }
  }

  Drupal.behaviors.autocomplete.attach = function (context) {

    var org = Drupal.autocomplete.options;
    // Act on textfields with the "form-autocomplete" class.
    var $autocomplete = $(once('autocomplete-alter', 'input.form-autocomplete', context));

    if ($autocomplete.length) {
      if( $autocomplete.hasClass('addsearch')){
        const orginalRender = $.ui.autocomplete;

        var orginalSelectHandler = Drupal.autocomplete.options.select;

        // Helper to create couple extra elements to make menu away.
        const renderMenu = function( ul, items ) {
          var nextColumn = false;

          $.each( items, ( index, item ) => {

            var el = this._renderItemData( nextColumn ? nextColumn : ul, item );

            if(!nextColumn && item.value === "" && item.label === "") {

              $('a', el).attr('data-content', Drupal.t('Search suggestions'));
              el.addClass('ui-menu-divider ui-widget-content')
              var ol = $('<ol>').appendTo(el);
              nextColumn = ol;

            }
            else {

              el.click(e => {
                $(this.element).val(item.value);
                $(
                  this.element).trigger('input')
              });
            }
          });

          const close = $('<li class="close">').appendTo(ul);

          close.click( e => {
            $(this.element).autocomplete("close");
          })
        }

        Drupal.autocomplete.options = Object.assign(
          {},
          Drupal.autocomplete.options,
          {
            position: {
              my: "left top",
              at: "left bottom",
              collision: "fit none"
            },
            classes: {
              "ui-autocomplete": "addsearch"
            },
            response: function( event, ui ) {
              const gridTpl = "\". s\"",
                    $el = $(this).autocomplete( 'widget'),
                    count = Math.max(0,ui.content.length - 1);

              $el.css('grid-template-areas', gridTpl.repeat(count) + "\"x x\"")

            },
            select: function selectHandler(event, ui) {
              if (validURL(ui.item.value)) {

                sendStats(drupalSettings.addsearch.pubkey,{
                   "action": "click",
                   "keyword": event.target.value,
                   "docid": ui.item.docid,
                   "position": ui.item.pos
                })

                window.location = ui.item.value;
                return false;
              }

              $(this).data('dontClose', true);

              return true;
            },
            // Add our own renderer.
            create: function( event, ui ) {
              $(this).data('ui-autocomplete')._renderMenu = renderMenu
            },

            focus: function( event, ui ) {
              var val = event.target.value,
                  divider = event.currentTarget.querySelector('.ui-menu-divider'),
                  previewEl = event.currentTarget.querySelector('.preview');

              if (ui.item.capture) {
                if (!previewEl) {
                  previewEl = document.createElement('div');
                  previewEl.innerHTML = '<img src="' + ui.item.capture + '" />';
                  previewEl.classList.add('preview')
                  divider.insertAdjacentElement('afterbegin',previewEl);
                }

                previewEl.querySelector('img').src = ui.item.capture;
              }

              return false;
            },

            close: function (event, ui) {

              if(typeof event.originalEvent !== 'undefined' && event.originalEvent.type === 'blur'){
                $(this).data('dontClose',false)
              }

              if($(this).data('dontClose')){
                event.target.focus();
                $(event.target).autocomplete( "widget" ).show()
                $(this).autocomplete( 'search' );
                if(typeof event.originalEvent == 'undefined') {
                  $(this).data('dontClose',false)
                }
              }

              $(event.target).autocomplete( "widget" ).find('.preview').remove()
            }
          }
        );
      }
    }
    func(context);

    Drupal.autocomplete.options = org;
  }

})(jQuery, drupalSettings);
