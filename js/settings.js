(function ($, Drupal) {
  Drupal.behaviors.contentTypes = {
    attach: function attach(context) {
      var $context = $(context);

      $context.find('details[id^=edit-bundles].vertical-tabs__pane').each( function(index, pane) {
        $(pane).drupalSetSummary(function (context) {
          var vals = [];
          $('input:checked[name$="[enabled]"]', context).next('label').each(function () {
            vals.push(Drupal.checkPlain("Enabled"));
          });

        return vals.join(', ');
        });
      })
    }
  };
})(jQuery, Drupal);
