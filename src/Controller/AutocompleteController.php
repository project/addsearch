<?php

namespace Drupal\addsearch\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request) {
    $results = [];
    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {

      $searchService = \Drupal::service('addsearch.restapiservice');

      $searchApiResult = json_decode(
        $searchService->suggest($input),
        TRUE
      )['suggestions'];

      $order = $request->query->get('order');

      // When there is custom field we need to split it bit to make it work.
      if ($customField = $request->query->get('customField')) {
        foreach ($customField as $field => $value) {
          if (is_array($value)) {
            foreach ($value as $orCondition) {
              $searchService->addCustomFieldToQuery($field, $orCondition);
            }
          }
          else {
            $searchService->addCustomFieldToQuery($field, $value);
          }
        }
      }


      if ($sort = $request->query->get('sort')) {
        $searchService->matchSort($sort);
      }

      if ($limit = $request->query->get('limit')) {
        $searchService->query['limit'] = $limit;
      }

      if ($lang = $request->query->get('lang')) {
        $searchService->query['lang'] = $lang;
      }

      $searchApiResults = json_decode(
        $searchService->makeSearch($input),
        TRUE
      )['hits'];

      $i = 0;
      foreach ($searchApiResults as $result) {
        $results[] = [
          'value' => $result['url'],
          'label' => $this->constructCoolLabel($result),
          'capture' => $result['images']['capture'],
          'docid' => $result['id'],
          'pos' => $i,
        ];
        $i++;
      }

      $results = array_merge($results, $this->createSuggestionTermList($searchApiResult));
      if (count($results) == 1) {
        $results = [];
      }
    }

    return new JsonResponse($results);
  }

  /**
   * Constructs cool label for suggestion.
   *
   * @param array $item
   *   Array of returned search result.
   *
   * @return string
   *   HTML formatted string.
   */
  protected function constructCoolLabel(array $item) {

    // default attributes
    $attributes = ['class' => ['result-type-' . $item['document_type']]];
    $images = $item['images'];
    
    // add type if set, aka promoted or something else
    if(isset($item['type'])) {
      
      $attributes['class'][] = strtolower($item['type']);
      if($item['type'] == 'PROMOTED') {
        $images['main'] = $item['style']['image_url']; 
      }
    }

    $renderItem = [
      '#theme' => 'addsearch_autocomplete_item',
      '#url' => $item["url"],
      '#images' => $images,
      '#attributes' => $attributes,
      '#result_type' => $item['document_type'],
      '#title' => $this->replaceSiteTitle($item['title']),
      '#highlight' => [
        '#markup' => $item['highlight'],
        '#allowed_tags' => ['em']
      ],
      '#categories' => $item['categories'],
    ];

    return \Drupal::service('renderer')->renderRoot($renderItem);
  }

  protected function replaceSiteTitle($str) {
    $siteName = \Drupal::config('system.site')->get('name');
    return preg_replace("/ \| " . preg_quote($siteName) . "/", "", $str);
  }

  protected function getCategory($cat) {

    if (count($cat) < 3) {
      return NULL;
    }

    return substr($cat[1], 2);
  }

  /**
   * Creates divider and suggestion array to autocomplete.
   *
   * @param array|NULL $suggestions
   *   Array of suggestion from rest api.
   *
   * @return array
   *   Array of item to be served to autocomplete.
   */
  protected function createSuggestionTermList($suggestions) {
    $results = [];
    $results[] = [
      'value' => '',
      'label' => '',
    ];

    foreach ($suggestions as $result) {
      $results[] = [
        'value' => $result['value'],
        'label' => $result['value'],
      ];
    }
    return $results;
  }

}
