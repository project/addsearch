<?php

namespace Drupal\addsearch;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7;

/**
 * Addsearch service class.
 */
class AddsearchService {
  use StringTranslationTrait;
  /**
   * Addsearch api base url.
   *
   * @var string
   */
  protected $apiBasePoint = "https://api.addsearch.com/";

  /**
   * Addsearch api version.
   *
   * @var string
   */
  protected $apiVersion = "v1";

  /**
   * Logger identifier.
   */
  const LOGGERID = 'Addsearch';

  /**
   * Configuration object.
   *
   * @var null||object
   */
  protected $config = NULL;

  /**
   * Http client.
   *
   * @var null|object
   */
  protected $client = NULL;

  /**
   * Entity type manager.
   *
   * @var null|object
   */
  protected $entityTypeManager = NULL;

  /**
   * Queryparams to construct query from.
   *
   * @var array
   */
  public $query = [
    'customField' => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->config = \Drupal::config('addsearch.settings');
    $this->client = \Drupal::httpClient();
    $this->entityTypeManager = \Drupal::entityTypeManager();
  }

  /**
   * Add sort types to query based on provided sort.
   *
   * @param string $val
   *   String which contains sort order asc || desc.
   */
  public function matchSort($val) {
    switch ($val) {
      case 'asc':
        $this->query['sort'] = 'date';
        $this->query['order'] = 'asc';
        break;

      case 'desc':
        $this->query['sort'] = 'date';
        $this->query['order'] = 'desc';
        break;

    }
  }

  /**
   * AddCustomfield to query params to Addsearch.
   *
   * @param string $field
   *   Custom field name.
   * @param string $value
   *   Custom field value.
   *
   * @return object
   *   Return this. instance.
   */
  public function addCustomFieldToQuery($field, $value) {
    $this->query['customField'][] = $field . '=' . $value;
    return $this;
  }

  /**
   * Helper to remove default params from query arr.
   */
  protected function cleanQueryParams() {
    foreach ($this->query as $key => $value) {
      switch ($key) {
        case 'page':
          if ($value == 1) {
            unset($this->query[$key]);
          }
          break;

        case 'limit':
          if ($value == 10) {
            unset($this->query[$key]);
          }
          break;

      }
    }
  }

  /**
   * Make post call to addsearch craweler to start crawling.
   *
   * @todo Need to add somepurge queue becouse the api rate limits are
   * 100req/15min.
   *
   * @param string $url
   *   Url which is wanted to be crawled.
   */
  public function crawlDocument($url) {

    $crawlerUrl = $this->constructBaseUrl() . 'crawler';

    $options = [
      "action" => "FETCH",
      "indexPublicKey" => $this->getPublicKey(),
      "url" => $url,
    ];

    $sendParams = [
      'auth' => [
        $this->getPublicKey(),
        $this->config->get('secret_api_key'),
      ],
      'body' => json_encode($options),
      'headers' => [
        'Accept'  => 'application/json',
        'Content-Type' => 'application/json',
      ],
    ];

    try {
      $request = $this->client->request('POST', $crawlerUrl, $sendParams);
      $body = '' . $request->getBody();

      return $body;
    }
    catch (\Exception $e) {
      $this->logRequestError($e);
      return FALSE;
    }

  }

  /**
   * Internal helper to make query, and hgandle responses.
   *
   * @param string $location
   *   Location where query is being made.
   *
   * @return string
   *   Request body, empty string when thre is error.
   */
  protected function makeQuery($location) {

    if (!isset($this->query['term']) || empty($this->query['term'])) {
      return "";
    }

    $this->cleanQueryParams();
    $str = $this->constructBaseUrl() . $location . '/' . $this->getPublicKey() . '?' . $this->consturctQuery();

    //dpm($str);
    try {
      $request = $this->client->get($str);
    }
    catch (\Exception $e) {
      $this->logRequestError($e);
      return "";
    }

    return $request->getBody();
  }

  /**
   * See Drupal\addsearch\AddsearchService::makeSearch.
   */
  public function search() {
    return $this->makeQuery('search');
  }

  /**
   * Construct suggest query.
   *
   * @param string $term
   *   Search term.
   *
   * @return string
   *   Response body, empty if there is error.
   */
  public function suggest($term) {
    $this->query['term'] = $term;
    return $this->makeQuery('suggest');
  }

  /**
   * Constructs search query.
   *
   * @param string $term
   *   Search term.
   *
   * @return string
   *   Response body, empty if there is error.
   */
  public function makeSearch($term) {
    $this->query['term'] = $term;
    return $this->makeQuery('search');
  }

  /**
   * Gets public key from configuration.
   *
   * @return string
   *   Api key string.
   */
  public function getPublicKey() {
    return $this->config->get('public_api_key');
  }

  /**
   * Contructs query params to url based on current query array.
   *
   * @return string
   *   Urlencoded param string.
   */
  protected function consturctQuery() {
    return implode('&', array_map(function ($key, $value) {
      // If value is array we need to add all the values to query with same key.
      if (is_array($value)) {
        $r = "";
        foreach ($value as $val) {
          $r .= "&" . $key . "=" . urlencode($val);
        }
        return mb_substr($r, 1);
      }
      else {
        $value = urlencode($value);
      }
      return $key . '=' . $value;
    }, array_keys($this->query), $this->query));
  }

  /**
   * Helper to contruct the base url for api calls.
   *
   * @return string
   *   Base for api call urls.
   */
  protected function constructBaseUrl() {
    return $this->apiBasePoint . $this->apiVersion . '/';
  }

  /**
   * Helper to write error log.
   *
   * @param object $e
   *   Execption returned from caller.
   */
  protected function logRequestError($e) {
    if ($e instanceof GuzzleException) {
      $msg = "ResponseBody:\n" . $e->getResponse()->toString();
      \Drupal::logger(self::LOGGERID)->error(preg_replace("/\\n/", '<br />', $msg));
    }
    // Becouse we r not sure of the error we print all of it.
    else {
      \Drupal::logger(self::LOGGERID)->error(print_r($e, TRUE));
    }
  }

  /**
   * Do we need to print labels or machine names.
   *
   * @return bool
   *   should we print or not.
   */
  public function printBundleLabels() {
    return !$this->config->get('machine_bundle_names');
  }

  public function getCustomFieldValue($entity, $entityName) {
    $overrides = $this->config->get('bundles');

    if(isset($overrides[$entityName]) && isset($overrides[$entityName]['groups']) && !empty($overrides[$entityName]['groups'])) {
      foreach ($overrides[$entityName]['groups'] as $group) {
        if (in_array($entity->bundle(), $group['options'])) {
          if ($this->printBundleLabels()) {
            return $this->t($group['name'], [], ['context' => 'addsearch_customfield']);
          }

          return $group['name'];
        }

      }

    }

    if ($this->printBundleLabels()) {
      return $this->_addsearch_get_bundle_name($entity);
    }

    return $entity->bundle();
  }

  protected function _addsearch_get_bundle_name($entity) {
    $bundle_type_id = $entity->getEntityType()->getBundleEntityType();

    $bundle = $this->entityTypeManager
      ->getStorage($bundle_type_id)
      ->load($entity->bundle());

    return $bundle->label();
  }

  protected function _addsearch_getBundleLabel($bundleId, $bundle) {
    if ($bundleId == 'node') {
      $bundleId = 'node_type';
    }
    return $this->entityTypeManager
      ->getStorage($bundleId)
      ->load($bundle)
      ->label();
  }

  public function getGroupsForBundle($bundle) {
    $bundles = $this->config->get('bundles');
    if (!$bundles) {
      return [];
    }

    $bundles = array_filter($bundles, function ($v) use ($bundle) {
      return !empty($v['enabled']) && $v['name'] == $bundle && !empty($v['groups']);
    });

    if (empty($bundles)) {
      return [];
    }

    $printLabels = $this->printBundleLabels();
    $r = [];
    foreach ($bundles as &$bundle) {
      foreach ($bundle['groups'] as $bk => &$group) {
        if ($printLabels) {
          $group['options'] = array_map(function ($name) {
            return $this->_addsearch_getBundleLabel('node', $name);
          }, $group['options']);
        }
        $r[$group['name']] = $group;
      }
    }
    return $r;
  }

  /**
   * Return all entities that r enabled.
   *
   * @return array
   *   Array of entities andtheir settings.
   */
  public function getEnabledBundles() {
    $bundles = $this->config->get('bundles');
    if (!$bundles) {
      return [];
    }

    return array_filter($bundles, function ($v) {
      return !empty($v['enabled']) && $v['name'] !== "";
    });
  }

  /**
   * Should we show orginal created time.
   *
   * @return bool
   *   Date using byt getCreatedTime function.
   */
  public function showPublishedTime() {
    return !empty($this->config->get('publish_time'));
  }

  /**
   * All the credits https://stackoverflow.com/questions/13036160/phps-array-map-including-keys/43004994#43004994.
   */
  public function arrayMapAssoc(callable $f, array $a) {
    return array_merge(...array_map($f, array_keys($a), $a));
  }

}
