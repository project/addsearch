<?php

namespace Drupal\addsearch\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\TextBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'autocomplete' element.
 *
 * @WebformElement(
 *   id = "addsearch_webform_autocomplete",
 *   label = @Translation("Addsearch autocomplete"),
 *   description = @Translation("Provides a text field element with auto completion."),
 *   category = @Translation("Advanced elements"),
 * )
 */
class AddsearchAutocomplete extends TextBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    $properties = [
      'route_params' => '',
    ] + parent::defineDefaultProperties()
      + $this->defineDefaultMultipleProperties();
    // Remove autocomplete property which is not applicable to
    // this autocomplete element.
    unset($properties['autocomplete']);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['route_params'] = [
      '#title' => $this->t('Route params in JSON format'),
      '#type' => 'textarea',
    ];


    return $form;
  }

  /****************************************************************************/

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    $routeParams = Json::decode($this->getElementProperty($element, 'route_params'));

    if ($routeParams === FALSE) {
      return;
    }

    $element['#type'] = 'textfield';
    $element['#autocomplete_route_name'] = 'addsearch.autocomplete';
    $element['#autocomplete_route_parameters'] = $routeParams;
    $element['#attributes']['class'][] = 'addsearch';
    $element['#attached']['library'][] = 'addsearch/addsearch_autocomplete';
    $element['#attached']['drupalSettings']['addsearch'] = [
      'pubkey' => \Drupal::service('addsearch.restapiservice')->getPublicKey(),
    ];
  }

}
