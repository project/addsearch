<?php

namespace Drupal\addsearch\Plugin\views\argument;

use Drupal\node\NodeStorageInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\argument\StringArgument;

/**
 * Argument handler to accept a node id.
 *
 * @ViewsArgument("term_arg_addsearch")
 */
class TermArgument extends StringArgument {
  /**
   * Build the query based upon the formula
   */
  public function query($group_by = FALSE) {
    $getValue = reset($this->value);

    $this->query->addWhere(0,'term', $getValue);
  }
}
