<?php

namespace Drupal\addsearch\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Serialization\Json;

/**
 * Class UrlToEntity.
 *
 * @ViewsField("addsearch_url_to_entity")
 */
class UrlToEntity extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['view_mode'] = ['default' => 'default'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['view_mode'] = [
      '#type' => 'select',
      '#options' => \Drupal::service('entity_display.repository')->getViewModeOptions('node'),
      '#title' => $this->t('View mode'),
      '#default_value' => $this->options['view_mode'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $entity = $this->getValue($values);
    $build = [];

    if ($entity && $entity instanceof NodeInterface) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
      $build = $view_builder->view($entity, $this->options['view_mode']);
    }
    else {
      $build = array_merge(
        ['#theme' => 'addsearch_failed_entity_callback'],
        $this->returnRenderArrFromResultArray($values)
      );
    }

    return $build;
  }

  protected function returnRenderArrFromResultArray(ResultRow $values) {
    $keys = [
      'url',
      'meta_description',
      'title',
      'images',
      'id',
      'categories',
      'custom_fields'
    ];

    $r = [];

    foreach ($keys as $value) {
      $r['#' . $value] = $values->{$value};
    }

    // Becouse this value contains html we want to render it with markup.
    $r['#highlight'] = [
      '#markup' => $values->highlight
    ];

    return $r;
  }

}
