<?php

namespace Drupal\addsearch\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Class Image.
 *
 * @ViewsField("addsearch_image")
 */
class Image extends FieldPluginBase {
  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['imagestyle'] = ['default' => 'default'];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $styles = ImageStyle::loadMultiple();
    $styles = array_map(function ($item) {
      return $item->label();
    }, $styles);

    $form['imagestyle'] = [
      '#type' => 'select',
      '#options' => $styles,
      '#title' => $this->t('View mode'),
      '#default_value' => $this->options['imagestyle'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {

    $r = [];

    $title = $this->getValue($values);

    if ($title) {

      $file = $this->getImageName($title['main']);

      // If image wasnt get to the drupal.
      if (!$file) {
        return $r;
      }

      // The image.factory service will check if our image is valid.
      $image = \Drupal::service('image.factory')->get($file);

      $variables = [];

      if ($image->isValid()) {
        $variables['width'] = $image->getWidth();

        $variables['height'] = $image->getHeight();
      }
      else {
        $variables['width'] = $variables['height'] = NULL;
      }

      $r = [
        '#theme' => 'image_style',
        '#style_name' => $this->options['imagestyle'],
        '#width' => $variables['width'],
        '#height' => $variables['height'],
        '#uri' => $file,
      ];
    }


    return $r;
  }

  /**
   * Helper to get correct name and place to save image.
   *
   * @param string $img
   *   Image name returned bu search api.
   *
   * @return string
   *   Drupal url for the file.
   */
  protected function getImageName($img) {
    $config = \Drupal::config('addsearch.settings');
    $ext = pathinfo($img, PATHINFO_EXTENSION);
    $fileName = pathinfo($img, PATHINFO_FILENAME);
    $fileStr = $config->get('image_save_location') . $fileName . '.' . $ext;
    $file = \Drupal::service('file_system')->getDestinationFilename($fileStr, 1);
    if (!file_exists($fileStr)) {
      $file = system_retrieve_file($img, $fileStr, TRUE);
      if (!$file) {
        return false;
      }
      $file = $file->getFileUri();
    }

    return $file;
  }

}
