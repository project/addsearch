<?php

namespace Drupal\addsearch\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Class Title.
 *
 * @ViewsField("addsearch_title")
 */
class Title extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    $options['remove_sitename'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['remove_sitename'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Remove sitename from title'),
      '#default_value' => $this->options['remove_sitename'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $r = [];
    $title = $this->getValue($values);
    if ($title) {
      if (is_array($title)) {
        $title = implode(', ', $title);
      }
      $r = [
        '#markup' => $this->options['remove_sitename'] ? $this->replaceTitle($title) : $title,
      ];
    }

    return $r;
  }

  /**
   * {@inheritdoc}
   */
  public function elementWrapperClasses($row_index = NULL) {
    return parent::elementWrapperClasses($row_index) . " palli";
  }

  protected function replaceTitle($title){
    $config = \Drupal::config('system.site');
    $name = $config->get('name');

    $pregQuotedSiteName = preg_quote($name);
    return preg_replace('/\ \|\ ' . $pregQuotedSiteName . '/', "", $title);

  }

}
