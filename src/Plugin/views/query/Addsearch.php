<?php

namespace Drupal\addsearch\Plugin\views\query;

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\Component\Utility\NestedArray;

/**
 * Query object to handle addsearch.
 *
 * @ViewsQuery(
 *   id = "addsearch_query",
 *   title = @Translation("Addsearch"),
 *   help = @Translation("Query against the addsearch API.")
 * )
 */
class Addsearch extends QueryPluginBase {

  const FIELD_PREFIX = 'customfield_';

  /**
   * Restapi service.
   *
   * @var null||object
   */
  protected $searchService = NULL;


  public $where = [];


  public $query = NULL;

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = []) {
    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    try {
      parent::init($view, $display, $options);
      $this->searchService = \Drupal::service('addsearch.restapiservice');
    }
    catch (\Exception $e) {

    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(ViewExecutable $view) {
    $this->view = $view;

    $this->searchService = \Drupal::service('addsearch.restapiservice');

    // Initialize the pager and let it modify the query to add limits. This has
    // to be done even for aborted queries since it might otherwise lead to a
    // fatal error when Views tries to access $view->pager.
    $view->initPager();
    $view->pager->query();

  }

  /**
   * Allow filters to add query params to provide better results.
   */
  public function addExtraArgumentToQuery($key, $value) {
    if (!isset($this->searchService->query[$key])) {
      $this->searchService->query[$key] = [];
    }
    if (!in_array($value, $this->searchService->query[$key], true)) {
      $this->searchService->query[$key][] = $value;
    }
  }

  /**
   * Helper to make query format correct based on filters.
   *
   * @param array $condition
   *   View condition to match from.
   */
  protected function matchConditions(array $condition) {

    $field_name = ltrim($condition['field'], '.');
    $val = $condition['value'];
    if (is_array($val)) {
      $val = reset($val);
    }
    if ($field_name === 'sort') {
      $this->searchService->matchSort($val);
    }
    elseif ($field_name === 'lang' && ($val == LanguageInterface::TYPE_INTERFACE || $val == LanguageInterface::TYPE_CONTENT)) {
      $this->searchService->query[$field_name] = \Drupal::languageManager()->getCurrentLanguage($val)->getId();
    }
    elseif (mb_strpos($field_name, $this::FIELD_PREFIX) !== FALSE) {
      $customType = mb_ereg_replace($this::FIELD_PREFIX, "", $field_name);

      $groups = $this->searchService->getGroupsForBundle($customType);

      if (isset($this->view->filter[$field_name]) && method_exists($this->view->filter[$field_name], 'normalizeQueryValue')) {
        $norm = $this->view->filter[$field_name]->normalizeQueryValue($condition['value']);

        $this->searchService->query['customField'] =
          NestedArray::mergeDeep(
            $this->searchService->query['customField'],
            $norm
          );
      }
      else {
        if ($val === 'none') {
          return;
        }
        foreach ($condition['value'] as $key => $value) {
          if ($value && $value !== 'none') {
            if (isset($groups[$value])) {
              foreach ($groups[$value]['options'] as $value) {
                $this->searchService->addCustomFieldToQuery($customType, $value);
              }
            }
            else {
              $this->searchService->addCustomFieldToQuery($customType, $value);
            }
          }
        }
      }
    }
    else {
      $filters[$field_name] = $val;
      $this->searchService->query[$field_name] = $val;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    return parent::defineOptions() + [
      'fuzzy' => [
        'default' => TRUE,
      ],
    ];
  }

  /**
   * Add settings for the ui.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['fuzzy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Fuzzy'),
      '#description' => $this->t('Also match words that are close to the defined keywords.'),
      '#default_value' => !empty($this->options['fuzzy']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {
    if (!empty($this->where)) {
      foreach ($this->where as $where_group => $where) {
        foreach ($where['conditions'] as $condition) {
          $this->matchConditions($condition);
        }
      }
    }

    if (!empty($this->options['fuzzy'])) {
      $this->searchService->query['fuzzy'] = 'auto';
    }

    try {

      $this->searchService->query['limit'] = $view->pager->getItemsPerPage();
      $this->searchService->query['page'] = $view->pager->current_page + 1;
      $response = $this->searchService->search();

    }
    catch (RequestException $e) {
      return;
    }

    // Empty request for malformed data.
    if (!$response) {
      return;
    }

    $keyword = $this->searchService->query['term'];

    $decodedResponse = json_decode($response, TRUE);
    $index = 0;

    $view->total_rows = $decodedResponse['total_hits'];

    if (!isset($decodedResponse['hits'])) {
      $view->total_rows = 0;
      return;
    }

    $pageOffset = $view->pager->current_page * $view->pager->getItemsPerPage();

    foreach ($decodedResponse['hits'] as $index => $hit) {

      $row = $hit;
      $row['index'] = $index;
      $row['position'] = $pageOffset + ($index + 1);
      $row['keyword'] = $keyword;
      $row['entity'] = $this->queryEntityFromUrl($row['url']);
      $view->result[] = new ResultRow($row);
    }

    // Trigger pager preExecute().
    $view->pager->preExecute($this->query);

    $view->pager->total_items = $decodedResponse['total_hits'];

    // Trigger pager postExecute().
    $view->pager->postExecute($view->result);
    $view->pager->updatePageInfo();

    $view->element['#attributes'] = [
      'data-addsearch-views-tracking' => 'true',
    ];
    $view->element['#attached']['drupalSettings']['addsearch'] = [
      'pubkey' => \Drupal::service('addsearch.restapiservice')->getPublicKey(),
    ];

    if (isset($decodedResponse['facets'])) {
      $view->element['#attached']['drupalSettings']['addsearch']['facetsToQuery'] = $decodedResponse['facets'];
    }

    $view->element['#attached']['library'][] = 'addsearch/addsearch_click';

  }



  /**
   * Get enabled languages.
   *
   * @return array
   *   Array of language ids.
   */
  protected function getLanguages() {
    return array_keys(\Drupal::languageManager()->getLanguages());
  }

  /**
   * Helper to determine entity from url.
   *
   * @param string $url
   *   Url what to find.
   *
   * @return Object|bool
   *   Entity or false if fails.
   */
  protected function queryEntityFromUrl($url) {
    $config = \Drupal::config('language.negotiation')
      ->get('url');

    $path = parse_url($url, PHP_URL_PATH);

    $langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();

    switch ($config['source']) {
      case LanguageNegotiationUrl::CONFIG_PATH_PREFIX:
        $path_args = explode('/', $path);

        $intersection = array_intersect($this->getLanguages(), $path_args);
        if (count($intersection)) {
          array_shift($path_args);
          $prefix = array_shift($path_args);
          $path = '/' . implode('/', $path_args);
          $langcode = $prefix;
        }

        break;
    }

    $route = \Drupal::service('path_alias.manager')->getPathByAlias($path, $langcode);
    $url = Url::fromUri("internal:" . $route);

    if ($url->isRouted()) {
      $params = $url->getRouteParameters();
      $arrayKeys = array_keys($params);
      $entityType = reset($arrayKeys);
      if (!$entityType || ($entityType !== 'node' && $entityType !== 'media')) {
        return FALSE;
      }

      return \Drupal::entityTypeManager()->getStorage($entityType)->load($params[$entityType]);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }
    $this->where[$group]['conditions'][] = [
      'field' => $field,
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * Adds a sort directive to this search query.
   *
   * If no sort is manually set, the results will be sorted descending by
   * relevance.
   *
   * @param string $field
   *   The field to sort by. The special fields 'search_api_relevance' (sort by
   *   relevance) and 'search_api_id' (sort by item id) may be used.
   * @param string $order
   *   The order to sort items in - either 'ASC' or 'DESC'.
   *
   * @return $this
   *
   * @see \Drupal\search_api\Query\QueryInterface::sort()
   */
  public function sort($field, $order = 'REL') {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addRelationship($alias) {
    $this->addEntityFromUrl = TRUE;
  }

}
