<?php

namespace Drupal\addsearch\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;


/**
 * Simple filter to handle filtering addsearch results by language.
 *
 * @ViewsFilter("addsearch_lang")
 */
class SearchLang extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public $no_operator = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {

    $itemsPerPage = isset($this->view->pager) ? $this->view->pager->getItemsPerPage() : 10;
    $queryParams = [
      'sort' => '',
      'order' => '',
      'limit' => $itemsPerPage,
      'lang' => '',
    ];

    $queryParams = array_merge(
      $queryParams,
      \Drupal::request()->query->all()
    );

    $langcodes = \Drupal::languageManager()->getLanguages();

    $langcodes = array_map(function ($item) {
      return $item->getName();
    }, $langcodes);


    $langcodes[LanguageInterface::TYPE_CONTENT] = $this->t('Content language');
    $langcodes[LanguageInterface::TYPE_INTERFACE] = $this->t('Interface language');

    $form['value'] = [
      '#type' => 'radios',
      '#options' => $langcodes,
      '#title' => $this->t('Value'),
      '#default_value' => $this->value,
    ];

  }

  public function getAutocompleteValue() {
    if ($this->value == LanguageInterface::TYPE_INTERFACE || $this->value == LanguageInterface::TYPE_CONTENT) {
      return \Drupal::languageManager()->getCurrentLanguage($this->value)->getId();
    }

    return $this->value;
  }

}
