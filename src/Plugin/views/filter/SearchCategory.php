<?php

namespace Drupal\addsearch\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple filter to handle filtering addsearch results by bundle.
 *
 * @ViewsFilter("addsearch_sort_by_bundle")
 */
class SearchCategory extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['enabled_bundles'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    $selections = $this->getCorrectBundleSelections(TRUE);
    unset($selections['none']);
    parent::buildOptionsForm($form, $form_state);

    $form['enabled_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => 'Visible sort bundles',
      '#options' => $selections,
      '#default_value' => $this->options['enabled_bundles'],
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $options = $this->getCorrectBundleSelections();

    // Disabled until got time to fix it.
    $multiple = FALSE;//$this->options['exposed'] ? $this->options['expose']['multiple'] : TRUE;

    $form['value'] = [
      '#type' => $multiple ? 'checkboxes' : 'radios',
      '#title' => 'Bundle',
      '#options' => $options,
      '#default_value' => $this->value,
    ];
  }

  /**
   * Helper to build correct right kinda filter for the bundle.
   *
   * @return array
   *   Array of values that can be used as filters.
   */
  protected function getCorrectBundleSelections($showAll = FALSE) {
    $r = [
      'none' => $this->t('All', [], ['context' => 'addsearch']),
    ];

    $customType = $this->getCustomTypeName();

    // Get needed settings from service.
    $addSearchService = \Drupal::service('addsearch.restapiservice');
    $printBundleLabels = $addSearchService->printBundleLabels();
    $bundleKey = $this->getThisEntity();

    // Merge wanted options.
    if ($bundleKey) {
      $bundleInfo = \Drupal::service('entity_type.bundle.info')->getBundleInfo($bundleKey);
      $r = array_merge($r,
        $addSearchService->arrayMapAssoc(function ($key, $val) use ($printBundleLabels) {
          if (!$printBundleLabels){
            return [$key => $this->t($val['label'], [], ['context' => 'addsearch'])];
          }
          else {
            return [$val['label'] => $this->t($val['label'], [], ['context' => 'addsearch'])];
          }
        }, $bundleInfo)
      );

      $r = array_merge($r, $this->getGroups($customType));

      // If we dont want to show all select options we will remove them from
      // sight.
      if (!$showAll) {
        $optionsForBundles = $this->options['enabled_bundles'];
        $r = array_filter($r, function ($item, $key) use ($optionsForBundles) {
          if ($key == 'none') {
            return TRUE;
          }
          $doestheymeet = isset($optionsForBundles[$key]) ? !empty($optionsForBundles[$key]) : FALSE;
          return $doestheymeet;
        },
        ARRAY_FILTER_USE_BOTH);
      }
    }

    return $r;

  }

  /**
   * Fetch groups and make them also selectable.
   */
  protected function getGroups($bundle) {
    $addSearchService = \Drupal::service('addsearch.restapiservice');
    $groups = $addSearchService->getGroupsForBundle($bundle);
    $r = [];

    foreach ($groups as $key => $group) {
      $r[$group['name']] = $this->t($group['name'], [], ['context' => 'addsearch']);
    }

    return $r;
  }

  /**
   * {@inheritdoc}
   */
  protected function makeOptions($val) {
    return $val['label'];
  }

  /**
   * Display the filter on the administrative summary.
   */
  public function adminSummary() {
    $multiple = $this->options['exposed'] ? $this->options['expose']['multiple'] : TRUE;

    return '';
  }

  /**
   * Make some translations to a form item to make it more suitable to exposing.
   */
  protected function exposedTranslate(&$form, $type) {
  }

  /**
   * Parse this type name.
   */
  protected function getCustomTypeName() {
    return mb_ereg_replace("customfield_", "", $this->realField);
  }

  /**
   * Find this field from settings.
   */
  protected function getThisEntity() {
    $customType = $this->getCustomTypeName();
    $addSearchService = \Drupal::service('addsearch.restapiservice');
    $enabledEntities = $addSearchService->getEnabledBundles();

    $enabledEntities = array_filter($enabledEntities, function ($v) use ($customType) {
      return mb_strtolower($v['name']) == $customType;
    });

    return empty($enabledEntities) ? FALSE : array_keys($enabledEntities)[0];
  }

  /**
   * Convert machine names if neded.
   */
  public function normalizeQueryValue($condition) {

    $bundleKey = $this->getThisEntity();
    if (!$bundleKey) {
      return "";
    }

    $r = [];
    $addSearchService = \Drupal::service('addsearch.restapiservice');
    $printBundleLabels = $addSearchService->printBundleLabels();
    $customType = $this->getCustomTypeName();
    $groups = $addSearchService->getGroupsForBundle($customType);

    $bundleInfo = \Drupal::service('entity_type.bundle.info')->getBundleInfo($bundleKey);

    if ($addSearchService->printBundleLabels()) {
      $convertValue = function ($val) use ($bundleInfo) {
        return isset($bundleInfo[$val]) ? $bundleInfo[$val]['label'] : $val;
      };
    }
    else {
      $convertValue = function ($val) {
        return $val;
      };
    }

    if (!is_array($condition)) {
      $r[] = $customType . '=' . $condition;
    }
    else {
      foreach ($condition as $key => $value) {
        if ($value && $value !== 'none') {
          $r[] = $customType . '=' . $value;
        }
      }
    }

    return $r;

  }

  public function getAutocompleteKey(){
    return 'customField';
  }

  public function getAutocompleteValue() {
    $val = 'none';
    $exposed = $this->view->getExposedInput();
    if (isset($exposed[$this->field])) {
      $val = $exposed[$this->field];
    }

    if ($val === 'none') {
      return NULL;
    }

    $ret = [];
    $ret[mb_ereg_replace('customfield_', "", $this->field)] = $val;

    return $ret;
  }

}
