<?php

namespace Drupal\addsearch\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple filter to handle filtering addsearch results by term.
 *
 * @ViewsFilter("addsearch_term")
 */
class SearchWord extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public $no_operator = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {

    $itemsPerPage = isset($this->view->pager) ? $this->view->pager->getItemsPerPage() : 10;
    $queryParams = [
      'sort' => '',
      'order' => '',
      'limit' => $itemsPerPage,
      'lang' => '',
    ];

    foreach ($this->view->filter as $key => $filter) {
      $newKey = $key;

      if (method_exists($filter, 'getAutocompleteKey')) {
        $newKey = $filter->getAutocompleteKey();
        if (!isset($queryParams[$newKey])) {
          $queryParams[$newKey] = [];
        }
      }
      if (method_exists($filter, 'getAutocompleteValue')) {
        $val = $filter->getAutocompleteValue();
        if (!empty($val)) {
          $val = is_array($val) ? $val : [$val];
          $newkey = is_array($queryParams[$newKey]) ? $queryParams[$newKey] : [$queryParams[$key]];
          $merged = array_merge($val, $newkey);
          $queryParams[$newKey] = $merged;
        }

      }
    }

    /*$queryParams = array_merge(
      $queryParams,
      \Drupal::request()->query->all()
    );*/

    $form['value'] = [
      '#type' => 'textfield',
      '#autocomplete_route_name' => 'addsearch.autocomplete',
      '#autocomplete_route_parameters' => $queryParams,
      '#title' => $this->t('Value'),
      '#size' => 30,
      '#attributes' => [
        'class' => [
          'addsearch',
        ],
        'data-bef-auto-submit-exclude' => '',
      ],
      '#default_value' => $this->value,
    ];

    $form['value']['#attached']['drupalSettings']['addsearch'] = [
      'pubkey' => \Drupal::service('addsearch.restapiservice')->getPublicKey(),
    ];
    $form['value']['#attached']['library'][] = 'addsearch/addsearch_autocomplete';
  }

}
