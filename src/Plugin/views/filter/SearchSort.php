<?php

namespace Drupal\addsearch\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Simple filter to handle sorting addsearch results by date/relevance.
 *
 * @ViewsFilter("addsearch_sort_type")
 */
class SearchSort extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $form['value'] = [
      '#type' => 'radios',
      '#title' => 'Sort order',
      '#options' => [
        'rel' => $this->t('Relevance',[], ['context' => 'addsearch']),
        'asc' => $this->t('Date Asc',[], ['context' => 'addsearch']),
        'desc' => $this->t('Date Desc',[], ['context' => 'addsearch']),
      ],
      '#default_value' => $this->value,
    ];
  }

  /**
   * Make some translations to a form item to make it more suitable to exposing.
   */
  protected function exposedTranslate(&$form, $type) {}

}
