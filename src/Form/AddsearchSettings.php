<?php

namespace Drupal\addsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Component\Utility\NestedArray;

/**
 * Configure Addsearch settings.
 */
class AddsearchSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'addsearch_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'addsearch.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('addsearch.settings');

    $form['configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('addsearch Configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,

      'publish_time' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use bundles published time in addsearch index.'),
        '#default_value' => $config->get('publish_time'),
      ],

      'public_api_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('Public api key to addsearch'),
        '#description' => $this->t('Public api key.'),
        '#default_value' => $config->get('public_api_key'),
      ],

      'image_save_location' => [
        '#type' => 'textfield',
        '#title' => $this->t('Location to save images'),
        '#description' => $this->t('Directery where images are saved through search api so they can be used with image styles, directory is not created automatically so remember to create and put right permissions to it.'),
        '#default_value' => $config->get('image_save_location'),
      ],

      'tabbed_bundles' => [
        '#type' => 'vertical_tabs',
        '#title' => t('Entity bundle groups'),
      ],

      'bundles' => $this->indexedBundleNames($form_state),
    ];

    if (\Drupal::moduleHandler()->moduleExists('addsearch_purge')) {
      $form['configuration']['crawler_auth'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Addsearch purger authentication'),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,

        'secret_api_key' => [
          '#type' => 'textfield',
          '#title' => $this->t('Secret api key from addsearch.'),
          '#description' => $this->t('Secret api key to used in crawler to make document reuquested to crawling.'),
          '#default_value' => $config->get('secret_api_key'),
        ],
      ];
    }
    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Gets the form part of the entitties/bundles.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Render Array of form.
   */
  protected function indexedBundleNames(FormStateInterface $form_state) {
    $config = $this->config('addsearch.settings');
    $r = [
      '#type' => 'fieldset',
      '#title' => $this->t('Types of entities to identify to Addserarch via custom fields.'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
      'machine_bundle_names' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Use machine names in custom fields.'),
        '#default_value' => $config->get('machine_bundle_names'),
        '#parents' => ['machine_bundle_names'],
      ],
    ];

    $bundles = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();

    foreach ($bundles as $bundle => $bundle_info) {
      // Just becouse if there is more than one type of bundles there is change
      // that we want to filter by them.
      if (is_array($bundle_info) && count($bundle_info) >= 2) {

        $check = $bundle . '_check';
        $checkBoxDefault = FALSE;
        $textBoxDefault = "";
        $defaults = $config->get('bundles');

        if ($defaults && isset($defaults[$bundle])) {
          $checkBoxDefault = $defaults[$bundle]['enabled'];
          $textBoxDefault = $defaults[$bundle]['name'];
        }

        $r[$bundle] = [
          '#parents' => ['bundles', $bundle],
          '#type' => 'details',
          '#title' => $bundle,
          'enabled' => [
            '#type' => 'checkbox',
            '#title' => $this->t("Enable @bundle.",['@bundle' => $bundle]),
            '#default_value' => $checkBoxDefault,
          ],
          'name' => [
            '#type' => 'textfield',
            '#title' => $this->t('CustomField property info.'),
            '#description' => $this->t('Adds custom field property named by this, and followed by bundle name.'),
            '#default_value' => $textBoxDefault,
            '#states' => [
              'visible' => [
                ':input[name="bundles[' . $bundle . '][enabled]"]' => [
                  'checked' => TRUE,
                ],
              ],
            ],
          ],
          'bundlegroups' => $this->bundleGroups($bundle, $bundle_info, $form_state, $form),
          '#group' => 'tabbed_bundles',
          '#tree' => TRUE,
        ];
      }
    }

    $enabledBundles = [];

    return $r;
  }

  /**
   * Addding grousp and their buttons.
   *
   * @param string $bundle
   *   Entity type name.
   * @param array $bundle_info
   *   Bundles from entity.
   * @param Drupal\Core\Form\FormStateInterface &$form_state
   *   Stat of the form.
   * @param array|null &$form
   *   Form itself.
   *
   * @return array
   *   RTender array.
   */
  protected function bundleGroups($bundle, array $bundle_info, FormStateInterface &$form_state, &$form) {
    $wrapper = $this->createElementId($bundle);
    $tree = [
      'bundles',
      $bundle,
      'groups',
    ];

    $values = $form_state->getValue($tree);
    $userInput = $form_state->getUserInput();
    $vals = NestedArray::getValue($userInput, $tree);

    if (!$values) {
      $values = [];
    }

    return [
      'groups' => [
        '#prefix' => '<div id="' . $wrapper . '">',
        '#suffix' => '</div>',
        'add' => $this->fieldSetForBundles($values, $bundle, $bundle_info, $form_state, $form),
      ],

      'addButton' => [
        '#type' => 'button',
        '#value' => $this->t("Add new group"),
        '#name' => $bundle . 'button',
        '#ajax' => [
          'callback' => '::addNewGroupAjaxCallback',
          'disable-refocus' => FALSE,
          'event' => 'click',
          'wrapper' => $wrapper,
          'progress' => [
            'type' => 'throbber',
            'message' => $wrapper,
          ],
        ],
        '#states' => [
          'visible' => [
            ':input[name="bundles[' . $bundle . '][enabled]"]' => [
              'checked' => TRUE,
            ],
          ],
        ],
      ],

      '#attached' => [
        'library' => [
          'addsearch/addsearch_settings',
        ],
      ],
      '#tree' => TRUE,
    ];
  }

  /**
   * Helper to provide options to drupal format.
   */
  protected function createOptionsToRenderArr($opts) {
    $r = [];
    foreach ($opts as $val) {
      if (empty($val)) {
        continue;
      }
      $r[$val] = $val;
    }

    return $r;
  }

  /**
   * Helper to provide fieldset item per entity type.
   */
  public function fieldSetForBundles($arr, $bundle, $bundle_info, $form_state, &$form) {
    $config = $this->config('addsearch.settings')->get('bundles');
    $groups = [];
    if ($config){
      $groups = NestedArray::getValue($config, [$bundle, 'groups']);
    }
    $printable = $arr;

    // If we have preconfigured groups, and this is initial load.
    if (is_array($groups) && empty($form_state->getTriggeringElement())) {
      $printable = array_merge($groups, $printable);
    }

    $r = [];

    // Make options printable.
    $options = \Drupal::service('addsearch.restapiservice')->arrayMapAssoc(function ($key, $val) {
      return [$key => $val['label']];
    }, $bundle_info);

    $i = 0;

    foreach ($printable as $key => &$value) {
      // Parents to determine position of form elements.
      $parents = ['bundles', $bundle, 'groups', $i];

      $r[] = [
        '#parents' => $parents,
        '#type' => 'details',
        '#open' => !($i < count($groups)),
        '#title' => $value['name'],
        'name' => [
          '#attributes' => [
            'name' => 'bundles[' . $bundle . '][groups][' . $i . '][name]',
          ],
          '#type' => 'textfield',
          '#title' => $this->t('Group identifier'),
          '#value' => $value['name'],
        ],
        'options' => [
          '#id' => implode('-', $parents),
          '#multiple' => TRUE,
          '#type' => 'checkboxes',
          '#title' => $this->t('What bundles are included in this group.'),
          '#options' => $options,
          '#value' => $this->createOptionsToRenderArr($value['options']),
          '#attributes' => [
            'name' => 'bundles[' . $bundle . '][groups][' . $i . '][options][]',
          ],
          '#parents' => $parents,
        ],

        '#tree' => TRUE,
      ];

      $i++;
    }

    return $r;
  }

  /**
   * Ajax callback to add new element to groups.
   */
  public function addNewGroupAjaxCallback(array &$form, FormStateInterface &$form_state) {

    // First we need triggering element tro determine elements.
    $triggered = $form_state->getTriggeringElement();
    $groupEl = $triggered['#parents'];

    // This is determination of the bundle we r editing.
    $bundle = $groupEl[1];

    // Get the user input and crwl value from there.
    $userInput = $form_state->getUserInput();
    $vals = NestedArray::getValue($userInput, ['bundles', $bundle, 'groups']);

    // Add new element as Ajax wants.
    $vals[] = [
      'name' => 'added',
      'options' => [],
    ];

    // Force form to refresh to get new build id.
    $form_state->setRebuild();

    // Get all the cool info to render new fieldsets.
    $bundles = \Drupal::service('entity_type.bundle.info')->getAllBundleInfo();
    $fieldsets = $this->fieldSetForBundles($vals, $bundle, $bundles[$bundle], $form_state, $form);

    // There is bugs in core when ajax dont render checkboxes correctly.
    // https://www.drupal.org/project/drupal/issues/2758631
    foreach ($fieldsets as $fieldsetsKey => &$value) {
      // First we render them.
      Checkboxes::processCheckboxes($value['options'], $form_state, $form);
      if (isset($userInput['bundles'][$bundle]['groups'])) {
        foreach ($userInput['bundles'][$bundle]['groups'] as $id => $elements) {
          if ($fieldsetsKey !== $id) {
            continue;
          }
          // And finally check it if needed.
          foreach ($elements['options'] as $opt) {
            if (isset($value['options'][$opt]) && !empty($opt)) {
              $fieldsets[$fieldsetsKey]['options'][$opt]['#checked'] = TRUE;
            }
          }
        }
      }

    }

    // Finally return the elements.
    return [
      '#prefix' => '<div id="' . $this->createElementId($bundle) . '">',
      '#suffix' => '</div>',
      '#parents' => [
        'bundles',
        $bundle,
        'groups',
      ],
      'add' => $fieldsets,
    ];
  }

  /**
   * Helper to create wrapper id from bundle name.
   */
  protected function createElementId($bundle) {
    $bundle = preg_replace("/_/", "", $bundle);
    return Html::cleanCssIdentifier('grouped-' . $bundle . '-bundles');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::service('config.factory')->getEditable('addsearch.settings');

    $config->set('public_api_key', $form_state->getValue('public_api_key'))->save();
    $config->set('image_save_location', $form_state->getValue('image_save_location'))->save();

    $config->set('machine_bundle_names', $form_state->getValue('machine_bundle_names'))->save();

    $form_state->setCleanValueKeys([['bundles', 'machine_bundle_names']]);

    if (isset($form_state->getUserInput()['bundles'])){
      $entityTypes = $form_state->getUserInput()['bundles'];
    }
    else {
      $entityTypes = $form_state->getValues()['bundles'];
    }

    // Format groups to settings suitable form.
    foreach ($entityTypes as $key => &$entity) {
      if($key == 'machine_bundle_names') {
        continue;
      }
      if (is_array($entity) && isset($entity['groups'])) {
        foreach ($entity['groups'] as $groupKey => $group) {
          if (!isset($group['options']) || empty($group['options'])) {
            unset($entity['groups'][$groupKey]);
          }
          else {
            $entity['groups'][$groupKey] = array_filter($group, function ($v, $k) {
              return in_array($k, ['name', 'options']);
            }, ARRAY_FILTER_USE_BOTH);
          }
        }
      }
    }

    $config->set('bundles', $entityTypes)->save();
    $config->set('publish_time', $form_state->getValue('publish_time'))->save();

    if (\Drupal::moduleHandler()->moduleExists('addsearch_purge')) {
      $config->set('sitekey', $form_state->getValue('sitekey'))->save();
      $config->set('secret_api_key', $form_state->getValue('secret_api_key'))->save();
    }

    parent::submitForm($form, $form_state);
  }

}
