# Addsearch

This module try to provide views integration between Drupal 8 and [AddSearch](https://www.addsearch.com/) via Addsearch Rest api. The module is trying to provide as close as possible out of the box experince similiar to AddSearch Components.
It also provides submodule to make instant crawling request to AddSearch Crawler when content is updated, so that your AddSearch index would be more on time to your current context of the site. To Use this feature you need patch the core [#2893933](https://www.drupal.org/project/drupal/issues/2893933), with queue expire behaviour.
