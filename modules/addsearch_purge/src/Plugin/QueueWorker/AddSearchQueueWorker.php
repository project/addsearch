<?php

/**
 * @file
 * Contains Drupal\addsearch_purge\Plugin\QueueWorker\AddSearchQueueWorker.php
 */

namespace Drupal\addsearch_purge\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Node Purger.
 *
 * @QueueWorker(
 *   id = "addsearch_queueworker",
 *   title = @Translation("Addsearch recrawl"),
 * )
 */
class AddSearchQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * Creates a new AddSearchQueueWorker object.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $node_storage
   *   The node storage.
   */
  public function __construct(EntityStorageInterface $node_storage) {
    $this->nodeStorage = $node_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity.manager')->getStorage('node')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $addsearchApi = \Drupal::service('addsearch.restapiservice');
    $addsearchApi->crawlDocument($data->url);
    \Drupal::state()->set('addsearch_crawl_api_limit', time() );
  }
}
